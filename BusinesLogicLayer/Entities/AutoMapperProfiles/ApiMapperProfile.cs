﻿using AutoMapper;
using BusinesLogicLayer.Entities.Api;
using ConfigurationProvider.Configurations.Implementations;

namespace BusinesLogicLayer.Entities.AutoMapperProfiles
{
    public class ApiMapperProfile : Profile
    {
        public ApiMapperProfile()
        {
            CreateMap<ApiConfiguration, ApiSettings>()
                .ForMember(x => x.AuthorizeApiEndpoint, x => x.MapFrom(c => c.AuthorizeApiEndpoint));
        }
    }
}
