﻿using System.Collections.Generic;

namespace BusinesLogicLayer.ModelsDto.Gdz
{
    public class GdzTableInfoDto
    {
        public List<SubjectDto> Subjects { get; set; }
    }
}