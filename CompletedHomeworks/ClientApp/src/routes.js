import React from "react";
import GdzTable from "./components/gdz-pages/gdz-table/gdz-table";
import {pageUrls} from "./common/constants"
import {Route, Switch} from 'react-router-dom';
import GdzSubject from "./components/gdz-pages/gdz-subject/gdz-subject";
import GdzGrade from "./components/gdz-pages/gdz-grade/gdz-grade";
import GdzTextbook from "./components/gdz-pages/gdz-textbook/gdz-textbook";
import GdzLesson from "./components/gdz-pages/gdz-lesson/lesson";
import GdzExercise from "./components/gdz-pages/gdz-exercise/exercise";

export default () => {
  return (
    <Switch>
      <Route path={pageUrls.URL_EXERCISE} render={() => <GdzExercise />}/>
      <Route path={pageUrls.URL_LESSON} render={() => <GdzLesson />}/>
      <Route path={pageUrls.URL_TEXTBOOK} render={() => <GdzTextbook />}/>
      <Route path={pageUrls.URL_GRADE} render={() => <GdzGrade />}/>
      <Route path={pageUrls.URL_SUBJECT} render={() => <GdzSubject />}/>
      <Route path={pageUrls.URL_GDZ_BASE} render={() => <GdzTable />}/>
    </Switch>
  );
};