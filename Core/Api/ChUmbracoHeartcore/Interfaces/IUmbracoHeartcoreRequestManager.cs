﻿using Common.Common.Enums;
using Core.Api.ChUmbracoHeartcore.Dto.Response;

namespace Core.Api.ChUmbracoHeartcore.Interfaces
{
    public interface IUmbracoHeartcoreRequestManager
    {
        DefaultUmbracoPageDto GetPageContent(CompletedHomeworksPages page);
    }
}