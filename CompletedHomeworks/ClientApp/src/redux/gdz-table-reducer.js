import {gdzApi} from "../api/tutor-api";

const actionTypes = {
  TOOGLE_PAGE_LOADING: 'TOOGLE_TABLE_PAGE_LOADING',
  SET_INFO_TO_GDZ_TABLE: 'SET_INFO_TO_GDZ_TABLE',
};

export const pageName = 'gdz-table';

const initialValue = {
  subjects: [
    /*
    {
      id: 1,
      title: 'Geography',
      urlTitle: 'geography',
      grades: [
        {
          id: 1,
          title: '1',
          urlTitle: 'first-grade',
          isActive: false
        },
        {
          id: 2,
          title: '2',
          urlTitle: 'second-grade',
          isActive: false
        },
        {
          id: 3,
          title: '3',
          urlTitle: 'third-grade',
          isActive: false
        },
        {
          id: 4,
          title: '4',
          urlTitle: 'fourth-grade',
          isActive: false
        },
        {
          id: 5,
          title: '5',
          urlTitle: 'fifth-grade',
          isActive: true
        },
        {
          id: 6,
          title: '6',
          urlTitle: 'sixth-grade',
          isActive: true
        },
        {
          id: 7,
          title: '7',
          urlTitle: 'seventh-grade',
          isActive: true
        },
        {
          id: 8,
          title: '8',
          urlTitle: 'eighth-grade',
          isActive: true
        },
        {
          id: 9,
          title: '9',
          urlTitle: 'ninth-grade',
          isActive: true
        },
        {
          id: 10,
          title: '10',
          urlTitle: 'tenth-grade',
          isActive: true
        },
        {
          id: 11,
          title: '11',
          urlTitle: 'eleventh-grade',
          isActive: true
        },
        {
          id: 12,
          title: '12',
          urlTitle: 'twelfth-grade',
          isActive: true
        }
      ]
    }
     */
  ],
  isFetching: true
};

export const GdzTableReducer = (state = initialValue, action) => {
  switch (action.type) {
    case actionTypes.TOOGLE_PAGE_LOADING:
      return {
        ...state,
        isFetching: action.isFetching
      };
    case actionTypes.SET_INFO_TO_GDZ_TABLE:
      return {
        ...state,
        ...action.data
      };
    default:
      return state;
  }
};

const isFetching = (isFetching) => {
  return {
    type: actionTypes.TOOGLE_PAGE_LOADING,
    isFetching
  };
};

const setInfoToGdzTable = (data) => {
  return {
    type: actionTypes.SET_INFO_TO_GDZ_TABLE,
    data
  };
};

export const getFilterTableInfo = () => {
  return (dispatch) => {
    dispatch(isFetching(true));
    gdzApi.getFilterTableInfo()
      .then(data => {
        dispatch(setInfoToGdzTable(data));
        dispatch(isFetching(false));
      })
  }
};

/*

const initialValue = {
  subjects: [
    {
      id: 1,
      title: 'Geography',
      urlTitle: 'geography'
    },
    {
      id: 2,
      title: 'Art',
      urlTitle: 'art'
    },
    {
      id: 3,
      title: 'History',
      urlTitle: 'history'
    },
    {
      id: 4,
      title: 'Music',
      urlTitle: 'music'
    },
    {
      id: 5,
      title: 'Natural history',
      urlTitle: 'natural-history'
    }
  ],
  grades: [
    {
      id: 1,
      title: '1',
      urlTitle: 'first-grade'
    },
    {
      id: 2,
      title: '2',
      urlTitle: 'second-grade'
    },
    {
      id: 3,
      title: '3',
      urlTitle: 'third-grade'
    },
    {
      id: 4,
      title: '4',
      urlTitle: 'fourth-grade'
    },
    {
      id: 5,
      title: '5',
      urlTitle: 'fifth-grade'
    },
    {
      id: 6,
      title: '6',
      urlTitle: 'sixth-grade'
    },
    {
      id: 7,
      title: '7',
      urlTitle: 'seventh-grade'
    },
    {
      id: 8,
      title: '8',
      urlTitle: 'eighth-grade'
    },
    {
      id: 9,
      title: '9',
      urlTitle: 'ninth-grade'
    },
    {
      id: 10,
      title: '10',
      urlTitle: 'tenth-grade'
    },
    {
      id: 11,
      title: '11',
      urlTitle: 'eleventh-grade'
    },
    {
      id: 12,
      title: '12',
      urlTitle: 'twelfth-grade'
    }
  ],
  textbooks: [
    {
      id: 1,
      title: 'TextBook 1',
      urlTitle: 'textbook-1',
      subjectId: 1,
      gradeId: 5
    },
    {
      id: 2,
      title: 'TextBook 2',
      urlTitle: 'textbook-2',
      subjectId: 1,
      gradeId: 6
    },
    {
      id: 3,
      title: 'TextBook 3',
      urlTitle: 'textbook-3',
      subjectId: 1,
      gradeId: 7
    },
    {
      id: 4,
      title: 'TextBook 4',
      urlTitle: 'textbook-4',
      subjectId: 1,
      gradeId: 8
    },
    {
      id: 5,
      title: 'TextBook 5',
      urlTitle: 'textbook-5',
      subjectId: 1,
      gradeId: 9
    },
    {
      id: 6,
      title: 'TextBook 6',
      urlTitle: 'textbook-6',
      subjectId: 1,
      gradeId: 10
    },
    {
      id: 7,
      title: 'TextBook 7',
      urlTitle: 'textbook-7',
      subjectId: 1,
      gradeId: 11
    },
    {
      id: 7,
      title: 'TextBook 7',
      urlTitle: 'textbook-7',
      subjectId: 1,
      gradeId: 12
    }
  ],
  lessons: [
    {
      id: 1,
      textbookId: 7,
      title: 'lesson 1',
      status: lessonStatuses.empty,
      lessonNumber: 1,
      videoLink: "https://www.youtube.com/watch?v=R6WSj0gGX30",
      result: {
        resultText: "lesson result"
      }
    },
    {
      id: 2,
      textbookId: 7,
      title: 'lesson 2',
      status: lessonStatuses.empty,
      lessonNumber: 2,
      videoLink: "https://www.youtube.com/watch?v=R6WSj0gGX30",
      result: {
        resultText: "lesson result"
      }
    },
    {
      id: 3,
      textbookId: 7,
      title: 'lesson 3',
      status: lessonStatuses.empty,
      lessonNumber: 3,
      videoLink: "https://www.youtube.com/watch?v=R6WSj0gGX30",
      result: {
        resultText: "lesson result"
      }
    }
  ],
  exercises: [
    {
      id: 1,
      lessonId: 1,
      title: "exercise title 1",
      condition: "condition 1",
      solution: "solution 1",
      advice: "advice 1",
      isNew: false
    }
  ],
  exerciseTrainers: [
    {
      id: 1,
      lessonId: 1,
      title: "exercise trainer title 1",
    },
    {
      id: 2,
      lessonId: 1,
      title: "exercise trainer title 2",
    }
  ],
}

*/


/*

 homeWorkFiltersInfo: {
    subjectFilterInfo: {
      title: "subjects",
      options: [
        {value: 'Art', label: 'Art'},
        {value: 'Geography', label: 'Geography'},
        {value: 'History', label: 'History'},
        {value: 'Music', label: 'Music'},
        {value: 'Natural history', label: 'Natural history'},
        {value: 'Science', label: 'Science'}
      ]
    },
    gradesFilterInfo: {
      title: "grades",
      options: [
        {value: 'first-grade', label: '1st'},
        {value: 'second-grade', label: '2nd'},
        {value: 'third-grade', label: '3rd'},
        {value: 'fourth-grade', label: '4th'},
        {value: 'fifth-grade', label: '5th'},
        {value: 'sixth-grade', label: '6th'},
      ]
    },
    textbookFilterInfo: {
      title: "textbooks",
      options: [
        {value: 'textbook-1', label: 'TextBook 1'},
        {value: 'textbook-2', label: 'TextBook 2'},
        {value: 'textbook-3', label: 'TextBook 3'},
        {value: 'textbook-4', label: 'TextBook 4'},
        {value: 'textbook-5', label: 'TextBook 5'},
        {value: 'textbook-6', label: 'TextBook 6'},
      ]
    },
    lessonFilterInfo: {
      title: "lessons",
      options: [
        {value: 'lesson-1', label: 'Lesson 1'},
        {value: 'lesson-2', label: 'Lesson 2'},
        {value: 'lesson-3', label: 'Lesson 3'},
        {value: 'lesson-4', label: 'Lesson 4'},
        {value: 'lesson-5', label: 'Lesson 5'},
        {value: 'lesson-6', label: 'Lesson 6'},
      ]
    }
  },

*/