import {gdzApi} from "../api/tutor-api";

const actionTypes = {
  TOOGLE_PAGE_LOADING: 'TOOGLE_SUBJECT_PAGE_LOADING',
  SET_INFO_SUBJECT_PAGE: 'SET_INFO_SUBJECT_PAGE',
};

export const pageName = 'gdz-subject';

const initialValue = {
  isFetching: true
};

export const GdzSubjectReducer = (state = initialValue, action) => {
  switch (action.type) {
    case actionTypes.TOOGLE_PAGE_LOADING:
      return {
        ...state,
        isFetching: action.isFetching
      };
    case actionTypes.SET_INFO_SUBJECT_PAGE:
      return {
        ...state,
        ...action.data
      };
    default:
      return state;
  }
};

const isFetching = (isFetching) => {
  return {
    type: actionTypes.TOOGLE_PAGE_LOADING,
    isFetching
  }
};

const setInfoToSubjectPage = (data) => {
  return {
    type: actionTypes.SET_INFO_SUBJECT_PAGE,
    data
  }
};

export const getInfoForSubjectPage = (subjectUrlTitle) => {
  return (dispatch) => {
    dispatch(isFetching(true));
    gdzApi.getInfoForSubjectPage(subjectUrlTitle)
      .then(data => {
        dispatch(setInfoToSubjectPage(data));
        dispatch(isFetching(false));
      })
  }
};