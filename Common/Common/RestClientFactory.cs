﻿using Common.Common.Interfaces;
using RestSharp;

namespace Common.Common
{
    public class RestClientFactory : IRestClientFactory
    {
        public IRestClient Create(string endpoint)
        {
            return new RestClient(endpoint);
        }
    }
}
