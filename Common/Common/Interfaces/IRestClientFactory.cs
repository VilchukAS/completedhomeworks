﻿using RestSharp;

namespace Common.Common.Interfaces
{
    public interface IRestClientFactory
    {
        IRestClient Create(string endpoint);
    }
}