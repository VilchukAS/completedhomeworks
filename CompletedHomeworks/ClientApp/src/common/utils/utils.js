import {pageUrls} from "../constants";

export default class Utils {
  static combineUrl(subjectUrlTitle, gradeUrlTitle, textbookUrlTitle, lessonUrlTitle, exerciseUrlTitle) {
    let url = pageUrls.URL_GDZ_BASE;
    const appendUrlPart = (url, urlTitle) => {
      if (Utils.isDef(urlTitle)) {
        url += `/${urlTitle}`;
      }
      return url;
    };

    url = appendUrlPart(url, subjectUrlTitle);
    url = appendUrlPart(url, gradeUrlTitle);
    url = appendUrlPart(url, textbookUrlTitle);
    url = appendUrlPart(url, lessonUrlTitle);
    url = appendUrlPart(url, exerciseUrlTitle);

    return url;
  }

  static isDef(value) {
    return typeof (value) !== 'undefined' && value !== null;
  }

  static getById = (arr, id) => {
    return arr.find((item) => {
      return item.id === id;
    })
  };

  static getByIds = (arr, ids) => {
    return arr.filter((item) => {
      return ids.some(id => {
        return item.id === id
      });
    })
  };

  static getParamsFromUrl(url) {
    const queryString = require('query-string');
    return queryString.parseUrl(url).query;
  }
}