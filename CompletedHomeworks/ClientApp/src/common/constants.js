export const routeParams = {
  subject: 'subject',
  grade: 'grade',
  textbook: 'textbook',
  lesson: 'lesson',
  exercise: 'exercise'
};

export const URL_THE_LONGEST_URL_TEMPLATE_FOR_BREADCRUMBS =`/gdz/:${routeParams.subject}?/:${routeParams.grade}?/:${routeParams.textbook}?/:${routeParams.lesson}?/:${routeParams.exercise}?`

export const pageUrls = {
  URL_GDZ_BASE: '/gdz',
  URL_SUBJECT: `/gdz/:${routeParams.subject}`,
  URL_GRADE: `/gdz/:${routeParams.subject}/:${routeParams.grade}`,
  URL_TEXTBOOK: `/gdz/:${routeParams.subject}/:${routeParams.grade}/:${routeParams.textbook}`,
  URL_LESSON: `/gdz/:${routeParams.subject}/:${routeParams.grade}/:${routeParams.textbook}/:${routeParams.lesson}`,
  URL_EXERCISE: `/gdz/:${routeParams.subject}/:${routeParams.grade}/:${routeParams.textbook}/:${routeParams.lesson}/:${routeParams.exercise}`
};

export const userRoles = {
  User: 1,
  Teacher: 2,
  Methodist: 3
};

export const lessonStatuses = {
  empty: 1,
  notVerified: 2,
  verified: 3
};

/* это нужно куда-то вынести и получать с сервака. но не в стейт. мб куки? // подумай... */
export const account = {
  FirstName: "Artem",
  LastName: "Vilchuk",
  role: userRoles.User
};