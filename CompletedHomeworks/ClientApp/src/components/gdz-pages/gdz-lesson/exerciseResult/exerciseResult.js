import React from "react";
import style from "./exerciseResult.module.css"

const ExerciseResult = (props) => {
  return (
    <div className={style.exerciseResult}>
      {props.exerciseResultInfo.someText}
    </div>
  )
}

export default ExerciseResult