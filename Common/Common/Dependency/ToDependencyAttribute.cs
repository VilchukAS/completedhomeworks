﻿using System;

namespace Common.Common.Dependency
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ToDependencyAttribute : DependencyAttribute
    {
        public ToDependencyAttribute(Type type) : base()
        {
            this.DependencyTarget = DependencyTarget.To;
            this.DependencyType = type;
        }

        public ToDependencyAttribute(Type type, string dependencyName) : this(type)
        {
            this.DependencyName = dependencyName;
        }
    }
}