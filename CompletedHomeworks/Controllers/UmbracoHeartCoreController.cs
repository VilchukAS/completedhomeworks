﻿using System;
using Common.Common.Enums;
using Core.Api.ChUmbracoHeartcore.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompletedHomeworks.Controllers
{
    [ApiController]
    [Route("api/v1/UmbracoHeartcore")]
    public class UmbracoHeartcoreController : Controller
    {
        private readonly IUmbracoHeartcoreRequestManager _umbracoHeartcoreRequestManager;

        public UmbracoHeartcoreController(IUmbracoHeartcoreRequestManager umbracoHeartcoreRequestManager)
        {
            _umbracoHeartcoreRequestManager = umbracoHeartcoreRequestManager;
        }

        [HttpGet("pages/{page}")]
        public ActionResult Get(int page)
        {
            if (!IsPageDefined(page)) 
                return NotFound();

            var pageInfo = _umbracoHeartcoreRequestManager.GetPageContent((CompletedHomeworksPages) page);
            return this.Json(pageInfo);
        }

        private bool IsPageDefined(int page)
        {
            return Enum.IsDefined(typeof(CompletedHomeworksPages), page);
        }
    }
}