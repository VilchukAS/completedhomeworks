﻿using System;
using System.Runtime.CompilerServices;

namespace Common.Common.Dependency
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InterfaceDependencyAttribute : DependencyAttribute
    {
        public InterfaceDependencyAttribute() : base()
        {
            DependencyTarget = DependencyTarget.Interface;
        }

        public InterfaceDependencyAttribute(string dependencyName) : this()
        {
            DependencyName = dependencyName;
        }
    }
}