﻿using System.IO;
using System.Reflection;

using ConfigurationProvider.Interfaces;

using Microsoft.Extensions.Configuration;

namespace ConfigurationProvider.Providers
{
    public class CoreConfigurationProvider : BaseConfigurationProvider
    {
        public CoreConfigurationProvider(IEnvironmentSettingsProvider environmentSettingsProvider)
            : base(environmentSettingsProvider)
        {
        }

        protected override string BasePath => Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    }
}
