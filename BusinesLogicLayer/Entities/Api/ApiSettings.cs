﻿namespace BusinesLogicLayer.Entities.Api
{
    public class ApiSettings
    {
        public string AuthorizeApiEndpoint { get; set; }
    }
}
