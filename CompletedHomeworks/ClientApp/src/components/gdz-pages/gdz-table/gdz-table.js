import React from "react";
import style from "./ghz-table.module.css"
import {Table} from "react-bootstrap";
import {NavLink, withRouter} from "react-router-dom";
import {getFilterTableInfo, pageName} from "../../../redux/gdz-table-reducer";
import {connect} from "react-redux";
import Placeholder from "../../shared/gdz-preloader/preloader";

class GdzTableContainer extends React.Component {
  componentDidMount() {
    this.props.getFilterTableInfo()
  }

  render() {
    let isFetching = this.props.isFetching;
    return (
      <>
        {
          isFetching
            ? <Placeholder/>
            : <GdzTable {...this.props} />
        }
      </>)
  }
}

const SubjectTableRow = (props) => {
  const gradeElements = props.grades.map((grade) => {
    return (
      <td key={grade.id}>
        <NavLink to={`/gdz/${props.urlTitle}/${grade.urlTitle}`}>
          {grade.title}
        </NavLink>
      </td>
    );
  });

  return (
    <tr>
      <td>
        <NavLink to={`/gdz/${props.urlTitle}`}>
          {props.title}
        </NavLink>
      </td>
      {gradeElements}
    </tr>
  );
};

const GdzTable = (props) => {
  const subjectElements = props.subjects.map((subject) => {
    return <SubjectTableRow key={subject.id} {...subject}/>
  });

  return (
    <div className={style.container}>
      <h3 className="mb-3">Готовые домашние задания</h3>
      <Table striped bordered hover size="sm">
        <thead>
        <tr>
          <th>Предмет</th>
          <th colSpan="12">Класс</th>
        </tr>
        </thead>
        <tbody>
        {subjectElements}
        </tbody>
      </Table>
    </div>
  );
};

export default connect(
  state => ({
    ...state[pageName]
  }),
  {
    getFilterTableInfo
  }
)(withRouter(GdzTableContainer));