import {applyMiddleware, combineReducers, createStore} from "redux";
import {GdzSubjectReducer, pageName as gdzSubjectPageName} from "./gdz-subject-reducer";
import {GdzTableReducer, pageName as gdzTablePageName} from "./gdz-table-reducer";
import {GdzGradeReducer, pageName as gdzGradePageName} from "./gdz-grade-reducer";
import {GdzTextbookReducer, pageName as gdzTextbookPageName} from "./gdz-textbook-reducer";
import {GdzLessonReducer, pageName as gdzLessonPageName} from "./gdz-lesson-reducer";
import {GdzExerciseReducer, pageName as gdzExercisePageName} from "./gdz-exercise-reducer";
import { reducer as formReducer } from 'redux-form'
import reduxThunkMiddleware from 'redux-thunk'


const reducers = combineReducers({
  [gdzTablePageName]: GdzTableReducer,
  [gdzSubjectPageName]: GdzSubjectReducer,
  [gdzGradePageName]: GdzGradeReducer,
  [gdzTextbookPageName]: GdzTextbookReducer,
  [gdzLessonPageName]: GdzLessonReducer,
  [gdzExercisePageName]: GdzExerciseReducer,
  form: formReducer
});
const store = createStore(reducers, applyMiddleware(reduxThunkMiddleware));
export default store;
window.store = store;