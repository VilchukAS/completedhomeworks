﻿using System.IO;
using ConfigurationProvider.Constants;
using ConfigurationProvider.Interfaces;

namespace ConfigurationProvider.Implementations
{
    public abstract class BaseEnvironmentSettingsProvider : IEnvironmentSettingsProvider
    {
        public string GetEnvironment()
        {
            var value = GetConfigurationValue(ConfigurationConstants.EnvironmentKey);

            return string.IsNullOrWhiteSpace(value) ? "Development" : value;
        }

        protected abstract string GetConfigurationValue(string key);
    }
}