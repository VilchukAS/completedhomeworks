import React from "react";
import style from "./back-button.module.css";
import backIcon from "../../../assets/images/back-icon.png";
import {NavLink} from "react-router-dom";

const BackButton = (props) => {
return (
  <div className={style.backContainer}>
    <NavLink className={style.back} to={props.to}>
      <img src={backIcon}/>{props.title}
    </NavLink>
  </div>
);

};

export default BackButton;