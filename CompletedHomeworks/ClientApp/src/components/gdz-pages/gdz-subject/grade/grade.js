import React from "react";
import style from './grade.module.css';
import Utils from "../../../../common/utils/utils";
import {NavLink} from "react-router-dom";

export default (props) => {
  const textbooks = props.textbooks.map(textbook =>
    <NavLink key={textbook.id}
             to={Utils.combineUrl(props.currentSubjectUrl, props.urlTitle, textbook.urlTitle)}>
      {textbook.title}, {textbook.author}
    </NavLink>
  );
  return (
    <div>
      <h5>{props.title} класс</h5>
      <div className={style.textbookContainer}>
        {textbooks}
      </div>
    </div>
  );
}