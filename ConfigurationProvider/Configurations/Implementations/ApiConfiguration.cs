﻿namespace ConfigurationProvider.Configurations.Implementations
{
    public class ApiConfiguration
    {
        public string AuthorizeApiEndpoint { get; set; }
    }
}
