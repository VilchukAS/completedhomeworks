﻿namespace ConfigurationProvider.Constants
{
    public class ConfigurationConstants
    {
        public static readonly string EnvironmentKey = "Environment";

        public static readonly char Separator = ';';

        public const string ConfigurationSectionTemplate = "{0}:{1}";

        public const string ConfigurationParameterTemplate = "{0}:{1}:{2}";
    }
}