import React from "react";
import {ErrorMessage, Field, FieldArray, Form, Formik} from 'formik';
import style from "./lesson.module.css";
import Thumb from "./thumb/thumb";
import ExerciseTrainer from "./exerciseTrainer/exerciseTrainer";
import Utils from "../../../common/utils/utils";
import Video from "./video/video";

const validate = values => {
  let errors = {};

  if (Utils.isDef(values.exercises) && values.exercises.length > 0) {
    const exercisesArrayErrors = [];
    values.exercises.forEach((exercise, ExerciseIndex) => {
      const exerciseErrors = {};
      if (exercise) {
        if (!exercise.exerciseTitle) {
          exerciseErrors.exerciseTitle = 'Required';
          exercisesArrayErrors[ExerciseIndex] = exerciseErrors
        }
        if (!exercise.condition) {
          exerciseErrors.condition = 'Required';
          exercisesArrayErrors[ExerciseIndex] = exerciseErrors
        }
        if (!exercise.solution) {
          exerciseErrors.solution = 'Required';
          exercisesArrayErrors[ExerciseIndex] = exerciseErrors
        }
        if (!exercise.advice) {
          exerciseErrors.advice = 'Required';
          exercisesArrayErrors[ExerciseIndex] = exerciseErrors
        }
      }
    });
    if (exercisesArrayErrors.length) {
      errors.exercises = exercisesArrayErrors
    }
  }

  if (!values.videoLink) {
    errors.videoLink = 'Required';
  }

  return errors;
};

const Lesson = (props) => {
  return (
    <Formik
      initialValues={{
        lessonId: props.id,
        exercises: props.exercises.map(exercise => ({
          id: exercise.id,
          exerciseTitle: exercise.exerciseTitle,
          condition: exercise.condition,
          conditionPhoto: exercise.conditionPhoto,
          solution: exercise.solution,
          solutionPhoto: exercise.solutionPhoto,
          advice: exercise.advice,
        })),
        videoLink: props.videoLink,
        exerciseTrainers: props.exerciseTrainers
      }}
      onSubmit={(values, {setSubmitting}) => {
        // invoke here your thunk instead setTimeout
        props.onSubmit(values)
      }}
      validate={validate}
    >
      {FormicComponent}
    </Formik>
  )
};

const FormicComponent = ({values, errors, touched, handleChange, handleBlur, isSubmitting, setFieldValue}) => {
  const exerciseTrainerElements = values.exerciseTrainers.map(
    (exerciseTrainer) => {
      return <ExerciseTrainer key={exerciseTrainer.id} {...exerciseTrainer}/>
    }
  );

  return (
    <Form>
      <FieldArray
        name="exercises"
        render={arrayHelpers => (
          <>
            {values.exercises && values.exercises.length > 0 &&
            values.exercises.map((exercise, index) => {
                const conditionPhotoRef = React.createRef();
                const solutionPhotoRef = React.createRef();
                return (
                  <div key={index} className={style.exercise}>

                    <button type="button" title="Удалить" onClick={() => arrayHelpers.remove(index)}>Удалить задачу
                    </button>

                    <div>
                      <label htmlFor={`exercises[${index}].exerciseTitle`}>Название задачи</label>
                      <Field placeholder="Введите название задачи" type="text"
                             name={`exercises[${index}].exerciseTitle`}/>
                      <ErrorMessage className={style.errorMessage} name={`exercises[${index}].exerciseTitle`}
                                    component="span"/>
                    </div>

                    <div>
                      <label htmlFor={`exercises[${index}].condition`}>Введите условие задачи</label>
                      <Field placeholder="Введите условие задачи" type="text" name={`exercises[${index}].condition`}/>
                      <ErrorMessage className={style.errorMessage} name={`exercises[${index}].condition`}
                                    component="span"/>
                    </div>

                    <div>
                      <label htmlFor={`exercises[${index}].conditionPhoto`}>Фото для условия</label>
                      <input
                        ref={conditionPhotoRef}
                        name={`exercises[${index}].conditionPhoto`}
                        type="file"
                        accept="image/*"
                        onChange={(event) => {
                          setFieldValue(`exercises[${index}].conditionPhoto`, event.currentTarget.files[0]);
                        }}/>
                      <div className={style.thumb}>
                        <Thumb file={values.exercises[index].conditionPhoto}/>
                        {values.exercises[index].conditionPhoto && <button onClick={() => {
                          conditionPhotoRef.current.value = null;
                          setFieldValue(`exercises[${index}].conditionPhoto`, null);
                        }}>Удалить изображение</button>}
                      </div>
                    </div>

                    <div>
                      <label htmlFor={`exercises[${index}].solution`}>Решение</label>
                      <Field placeholder="Введите решение задачи" type="text" name={`exercises[${index}].solution`}/>
                      <ErrorMessage className={style.errorMessage} name={`exercises[${index}].solution`}
                                    component="span"/>
                    </div>

                    <div>
                      <label htmlFor={`exercises[${index}].solutionPhoto`}>Фото для решения</label>
                      <input
                        ref={solutionPhotoRef}
                        name={`exercises[${index}].solutionPhoto`}
                        type="file"
                        accept="image/*"
                        onChange={(event) => {
                          setFieldValue(`exercises[${index}].solutionPhoto`, event.currentTarget.files[0]);
                        }}/>
                      <div className={style.thumb}>
                        <Thumb file={values.exercises[index].solutionPhoto}/>
                        {values.exercises[index].solutionPhoto && <button onClick={() => {
                          solutionPhotoRef.current.value = null;
                          setFieldValue(`exercises[${index}].solutionPhoto`, null);
                        }}>Удалить изображение</button>}
                      </div>
                    </div>

                    <div>
                      <label htmlFor={`exercises[${index}].advice`}>Совет</label>
                      <Field placeholder="Введите совет для решения задачи" type="text"
                             name={`exercises[${index}].advice`}/>
                      <ErrorMessage className={style.errorMessage} name={`exercises[${index}].advice`} component="span"/>
                    </div>

                  </div>
                )
              }
            )}
            <button
              type="button"
              onClick={() => arrayHelpers.push({
                id: "",
                exerciseTitle: "",
                condition: "",
                conditionPhoto: "",
                solution: "",
                solutionPhoto: "",
                advice: ""
              })}
            >
              Добавить упражнение
            </button>
          </>
        )}
      />

      <div>
        <label htmlFor="videoLink">Видео</label>
        <Field type="text" name="videoLink" placeholder="Укажите видео для урока"/>
        <ErrorMessage className={style.errorMessage} name="videoLink" component="span"/>
      </div>

      {exerciseTrainerElements}

      <button type="submit" disabled={isSubmitting}>
        Submit
      </button>

      <TempInfo values={values} errors={errors}/>
    </Form>
  );
};

const TempInfo = (props) => {
  return (
    <pre>
    {JSON.stringify(props, null, 2)}
  </pre>
  )
};

export default Lesson;