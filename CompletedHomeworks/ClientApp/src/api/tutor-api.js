import axios from "axios"
import {lessonStatuses} from "../common/constants";

const gdzAxiosInstance = axios.create({
  baseURL: 'http://local.completedhomeworks:90/api/v1/HomeWork'
});

export class gdzApi {
  static getFilterTableInfo() {
    return gdzAxiosInstance.get(`filter-table`)
      .then(response => response.data)
      .catch(err => console.log(err));
  }

  static getInfoForSubjectPage(subjectUrlTitle) {
    // return gdzAxiosInstance.get(`/subject`, {
    //     params: {
    //       subjectUrlTitle: subjectUrlTitle
    //     }
    //   })
    //   .then(response => response.data)
    //   .catch(err => console.log(err));
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          subjectTitle: 'География',
          id: 1,
          grades: [
            {
              id: 1,
              title: '1',
              urlTitle: 'first-grade',
              textbooks: [
                {
                  id: 1,
                  title: 'Название книги 1',
                  urlTitle: 'textbook-1',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 2,
              title: '2',
              urlTitle: 'second-grade',
              textbooks: [
                {
                  id: 2,
                  title: 'Название книги 2',
                  urlTitle: 'textbook-2',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 3,
              title: '3',
              urlTitle: 'third-grade',
              textbooks: [
                {
                  id: 3,
                  title: 'Название книги 3',
                  urlTitle: 'textbook-3',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 4,
              title: '4',
              urlTitle: 'fourth-grade 4',
              textbooks: [
                {
                  id: 4,
                  title: 'Название книги 4',
                  urlTitle: 'textbook-4',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 5,
              title: '5',
              urlTitle: 'fifth-grade',
              textbooks: [
                {
                  id: 5,
                  title: 'Название книги 5',
                  urlTitle: 'textbook-5',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 6,
              title: '6',
              urlTitle: 'sixth-grade',
              textbooks: [
                {
                  id: 6,
                  title: 'Название книги 6',
                  urlTitle: 'textbook-6',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 7,
              title: '7',
              urlTitle: 'seventh-grade',
              textbooks: [
                {
                  id: 7,
                  title: 'Название книги 7',
                  urlTitle: 'textbook-7',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 8,
              title: '8',
              urlTitle: 'eighth-grade',
              textbooks: [
                {
                  id: 8,
                  title: 'Название книги 8',
                  urlTitle: 'textbook-8',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 9,
              title: '9',
              urlTitle: 'ninth-grade',
              textbooks: [
                {
                  id: 9,
                  title: 'Название книги 9',
                  urlTitle: 'textbook-9',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 10,
              title: '10',
              urlTitle: 'tenth-grade',
              textbooks: [
                {
                  id: 10,
                  title: 'Название книги 10',
                  urlTitle: 'textbook-10',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 11,
              title: '11',
              urlTitle: 'eleventh-grade',
              textbooks: [
                {
                  id: 11,
                  title: 'Название книги 11',
                  urlTitle: 'textbook-11',
                  author: 'Фио автора'
                }
              ]
            },
            {
              id: 12,
              title: '12',
              urlTitle: 'twelfth-grade',
              textbooks: [
                {
                  id: 12,
                  title: 'Название книги 12',
                  urlTitle: 'textbook-12',
                  author: 'Фио автора'
                }
              ]
            }
          ]
        });
      }, 300);
    });
  }

  static getInfoForGradePage(subjectUrlTitle, gradeUrlTitle) {
    // return gdzAxiosInstance.get(`/grade`, {
    //     params: {
    //       subjectUrlTitle: subjectUrlTitle,
    //       gradeUrlTitle: gradeUrlTitle
    //     }
    //   })
    //   .then(response => response.data)
    //   .catch(err => console.log(err));
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          id: 1,
          pageHeader: 'Учебники по предмету "География" , 5 класс', //строка на серваге генерируется
          urlTitle: 'first-grade',
          gradeTitle: '5 класс',
          textbooks: [
            {
              id: 1,
              title: 'Название учебника 1',
              author: 'Фио автора 1',
              urlTitle: 'textbook-1',
              textbookImgSrc: 'https://www.vaco.ru/upload/iblock/779/7790ca82c0c643c9f90533e95bf1abe0.jpg'
            },
            {
              id: 2,
              title: 'Название учебника 2',
              urlTitle: 'textbook-2',
              author: 'Фио автора 2',
              textbookImgSrc: 'https://cv01.twirpx.net/1240/1240031.jpg?t=20170302180119'
            },
            {
              id: 3,
              title: 'Название учебника 3',
              author: 'Фио автора 3',
              urlTitle: 'textbook-3',
              textbookImgSrc: 'https://amital.ru/image/cache//data/import_files/c0/c0b1fadd-00b4-11e6-bcf8-00215aaa7db4-200x300.jpeg'
            }, ,
            {
              id: 4,
              title: 'Название учебника 4',
              author: 'Фио автора 4',
              urlTitle: 'textbook-4',
              textbookImgSrc: 'https://cdn1.ozone.ru/multimedia/1013761198.jpg'
            },]
        });
      }, 300);
    })
  }

  static getInfoForTextbookPage(subjectUrlTitle, gradeUrlTitle, textbookUtlTitle) {
    // return gdzAxiosInstance.get(`/textbook`, {
    //     params: {
    //       subjectUrlTitle: subjectUrlTitle,
    //       gradeUrlTitle: gradeUrlTitle,
    //       textbookUtlTitle: textbookUtlTitle
    //     }
    //   })
    //   .then(response => response.data)
    //   .catch(err => console.log(err));
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          id: 1,
          textbookTitle: 'Название учебника',
          subjectTitle: 'География',
          gradeTitle: '5 класс',
          urlTitle: 'textbook-1',
          author: 'ФИО автора',
          lessons: [
            {
              id: 1,
              title: 'География в древности',
              urlTitle: 'lesson-1',
              status: lessonStatuses.verified,
              lessonNumber: 1
            },
            {
              id: 2,
              title: 'Географические знания в Древней Европе',
              urlTitle: 'lesson-2',
              status: lessonStatuses.verified,
              lessonNumber: 2
            },
            {
              id: 3,
              title: 'География в эпоху Средневековья',
              urlTitle: 'lesson-3',
              status: lessonStatuses.notVerified,
              lessonNumber: 3
            },
            {
              id: 4,
              title: 'Эпоха Великих географических открытий',
              urlTitle: 'lesson-3',
              status: lessonStatuses.notVerified,
              lessonNumber: 4
            },
            {
              id: 5,
              title: 'Открытие Австралии и Антарктиды',
              urlTitle: 'lesson-3',
              status: lessonStatuses.empty,
              lessonNumber: 5
            }
          ]
        });
      }, 300);
    })
  }

  static getLessonInfoPage(subjectUrlTitle, gradeUrlTitle, textbookUrlTitle, lessonUrlTitle) {
    // return gdzAxiosInstance.get(`/textbook`, {
    //     params: {
    //       subjectUrlTitle,
    //       gradeUrlTitle,
    //       textbookUrlTitle,
    //       lessonUrlTitle
    //     }
    //   })
    //   .then(response => response.data)
    //   .catch(err => console.log(err));
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          id: 1,
          title: "Солнечная система",
          textbookTitle: "Изучение солнечной системы, Ванделеева Н.И.",
          subjectTitle: "География",
          gradeTitle: "5 класс",
          lessonTitle: "Название урока 777",
          exercises: [
            {
              id: 1,
              exerciseTitle: "Небесные тела",
              urlTitle: "exercise-title-1",
              condition: "Какой объект относится к небесным телам?",
              solution: "Планета",
              advice: "Внимательно подумай!",
              conditionPhoto: null,
              solutionPhoto : null
            },
            {
              id: 2,
              exerciseTitle: "Свойства метеоритов",
              urlTitle: "exercise-title-2",
              condition: "Какие свойства и признаки характерны для метеоритов?",
              solution: "Метеориты образуются из скоплений космической пыли",
              advice: "Нужно вспомнить что такое метеорит",
              conditionPhoto: null,
              solutionPhoto : null
            }
          ],
          videoLink: "https://www.youtube.com/watch?v=IUN664s7N-c",
          exerciseTrainers: [
            {
              id: 1,
              lessonId: 1,
              title: "Тренажёр 1",
            },
            {
              id: 2,
              lessonId: 1,
              title: "Тренажёр 2",
            }
          ],
          exerciseResult: {
            someText: "Рузельтат и рекомендации"
          }
        });
      }, 300);
    })
  }

  static getExerciseInfoPage(subjectUrlTitle, gradeUrlTitle, textbookUrlTitle, lessonUrlTitle, exerciseUrlTitle) {
    // return gdzAxiosInstance.get(`/textbook`, {
    //     params: {
    //       subjectUrlTitle,
    //       gradeUrlTitle,
    //       textbookUrlTitle,
    //       lessonUrlTitle,
    //       exerciseUrlTitle
    //     }
    //   })
    //   .then(response => response.data)
    //   .catch(err => console.log(err));
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          id: 1,
          exerciseTitle: "exercise title 1",
          urlTitle: "exercise-title-1",
          condition: "condition 1",
          solution: "solution 1",
          advice: "advice 1",
          isNew: false
        });
      }, 300);
    })
  }

  static getInitInfoForNavigation() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          grades: [
            {
              id: 1,
              title: '1 класс',
              urlTitle: 'first-grade',
            },
            {
              id: 2,
              title: '2 класс',
              urlTitle: 'second-grade'
            },
            {
              id: 3,
              title: '3 класс',
              urlTitle: 'third-grade'
            },
            {
              id: 4,
              title: '4 класс',
              urlTitle: 'fourth-grade'
            },
            {
              id: 5,
              title: '5 класс',
              urlTitle: 'fifth-grade'
            },
            {
              id: 6,
              title: '6 класс',
              urlTitle: 'sixth-grade'
            },
            {
              id: 7,
              title: '7 класс',
              urlTitle: 'seventh-grade'
            },
            {
              id: 8,
              title: '8 класс',
              urlTitle: 'eighth-grade'
            },
            {
              id: 9,
              title: '9 класс',
              urlTitle: 'ninth-grade'
            },
            {
              id: 10,
              title: '10 класс',
              urlTitle: 'tenth-grade'
            },
            {
              id: 11,
              title: '11 класс',
              urlTitle: 'eleventh-grade'
            },
            {
              id: 12,
              title: '12 класс',
              urlTitle: 'twelfth-grade'
            }
          ],
          subjects: [
            {
              id: 1,
              title: 'География',
              urlTitle: 'geography'
            },
            {
              id: 2,
              title: 'ИЗО',
              urlTitle: 'art'
            },
            {
              id: 3,
              title: 'Математика',
              urlTitle: 'math'
            },
            {
              id: 4,
              title: 'Музыка',
              urlTitle: 'music'
            },
            {
              id: 5,
              title: 'Всемирная история',
              urlTitle: 'natural-history'
            }
          ],
          gradesToSubjects: [
            {gradeId: 1, subjectIds: [1, 2, 3]},
            {gradeId: 2, subjectIds: [1, 2, 3]},
            {gradeId: 3, subjectIds: [1, 2, 3]},
            {gradeId: 4, subjectIds: [1, 2, 3]},
            {gradeId: 5, subjectIds: [1, 2, 3]},
            {gradeId: 6, subjectIds: [1, 2, 3]},
            {gradeId: 7, subjectIds: [1, 2, 3]},
            {gradeId: 8, subjectIds: [1, 2, 3]},
            {gradeId: 9, subjectIds: [1, 2, 3]},
            {gradeId: 10, subjectIds: [1, 2, 3]},
            {gradeId: 11, subjectIds: [1, 2, 3]},
            {gradeId: 12, subjectIds: [1, 2, 3, 4, 5]}
          ]
        });
      }, 300);
    })
  }

  static requestSelectedItemsNames(currentRouteParams) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          subject: "География",
          grade: "1 Класс",
          textbook: "Аванасьева",
          lesson: "",
          exercise: ""
        });
      }, 300);
    })
  }

  static saveLesson(formData){
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          resultCode: 200
        });
      }, 300);
    })
  }
}