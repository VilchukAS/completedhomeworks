﻿using System;
using System.Collections.Generic;
using System.Text;
using BusinesLogicLayer.Interfaces;
using BusinesLogicLayer.ModelsDto.Gdz;

namespace BusinesLogicLayer.Implementation
{
    public class HomeWorkService : IHomeWorkService
    {
        public bool IsLessonDefined(int lessonId)
        {
            //реализовать логику.надо заинджектить сюда репозиторий и в нём прорить наличие урока.
            return true;
        }

        public GdzTableInfoDto GetGdzTableInfo()
        {
            return new GdzTableInfoDto()
            {
                Subjects = new List<SubjectDto>()
                {
                    new SubjectDto()
                    {
                        Id = 1,
                        Title = "География",
                        UrlTitle = "geography",
                        Grades = new List<GradeDto>()
                        {
                            new GradeDto()
                            {
                                Id = 1,
                                Title = "1",
                                UrlTitle = "first-grade",
                                IsActive = false
                            },
                            new GradeDto()
                            {
                                Id = 2,
                                Title = "2",
                                UrlTitle = "second-grade",
                                IsActive = false
                            },
                            new GradeDto()
                            {
                                Id = 3,
                                Title = "3",
                                UrlTitle = "third-grade",
                                IsActive = false
                            },
                            new GradeDto()
                            {
                                Id = 4,
                                Title = "4",
                                UrlTitle = "fourth-grade",
                                IsActive = false
                            },
                            new GradeDto()
                            {
                                Id = 5,
                                Title = "5",
                                UrlTitle = "fifth-grade",
                                IsActive = true
                            },
                            new GradeDto()
                            {
                                Id = 6,
                                Title = "6",
                                UrlTitle = "sixth-grade",
                                IsActive = true
                            },
                            new GradeDto()
                            {
                                Id = 7,
                                Title = "7",
                                UrlTitle = "seventh-grade",
                                IsActive = true
                            },
                            new GradeDto()
                            {
                                Id = 8,
                                Title = "8",
                                UrlTitle = "eighth-grade",
                                IsActive = true
                            },
                            new GradeDto()
                            {
                                Id = 9,
                                Title = "9",
                                UrlTitle = "ninth-grade",
                                IsActive = true
                            },
                            new GradeDto()
                            {
                                Id = 10,
                                Title = "10",
                                UrlTitle = "tenth-grade",
                                IsActive = true
                            },
                            new GradeDto()
                            {
                                Id = 11,
                                Title = "11",
                                UrlTitle = "eleventh-grade",
                                IsActive = true
                            },
                            new GradeDto()
                            {
                                Id = 12,
                                Title = "12",
                                UrlTitle = "twelfth-grade",
                                IsActive = true
                            }
                        }
                        
                    }
                }
            };
        }
    }
}