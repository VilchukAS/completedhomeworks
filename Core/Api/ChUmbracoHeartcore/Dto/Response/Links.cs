﻿using Newtonsoft.Json;

namespace Core.Api.ChUmbracoHeartcore.Dto.Response
{
    public class Links
    {
        [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
        public Ancestors Self { get; set; }

        [JsonProperty("root", NullValueHandling = NullValueHandling.Ignore)]
        public Parent Root { get; set; }

        [JsonProperty("children", NullValueHandling = NullValueHandling.Ignore)]
        public Ancestors Children { get; set; }

        [JsonProperty("ancestors", NullValueHandling = NullValueHandling.Ignore)]
        public Ancestors Ancestors { get; set; }

        [JsonProperty("descendants", NullValueHandling = NullValueHandling.Ignore)]
        public Ancestors Descendants { get; set; }

        [JsonProperty("parent", NullValueHandling = NullValueHandling.Ignore)]
        public Parent Parent { get; set; }
    }
}