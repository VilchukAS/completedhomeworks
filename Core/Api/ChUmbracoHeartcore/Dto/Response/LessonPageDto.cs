﻿using Newtonsoft.Json;

namespace Core.Api.ChUmbracoHeartcore.Dto.Response
{
    public class LessonPageDto
    {
        [JsonProperty("title")] 
        public string Title { get; set; }
    }
}