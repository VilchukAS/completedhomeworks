﻿using System;

namespace Common.Common.Dependency
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DependencyAttribute : Attribute
    {
        public DependencyAttribute()
        {
            // NOTE: this is default application scope
            this.DependencyScope = DependencyScope.Singleton;
        }

        public DependencyScope DependencyScope { get; protected set; }

        public DependencyTarget DependencyTarget { get; protected set; }

        public Type DependencyType { get; protected set; }

        public string DependencyName { get; protected set; }
    }
}