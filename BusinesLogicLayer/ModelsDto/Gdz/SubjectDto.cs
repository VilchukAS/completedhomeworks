﻿using System.Collections.Generic;

namespace BusinesLogicLayer.ModelsDto.Gdz
{
    public class SubjectDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string UrlTitle { get; set; }
        public List<GradeDto> Grades { get; set; }
    }
}