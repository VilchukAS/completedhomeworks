﻿using System;
using Common.Common.Attributes;
using Common.Common.Enums;

namespace Common.Common.Extensions
{
    public static class CompletedHomeworksPagesExtension
    {
        public static string GetCarrierUniqueIdentifier(this CompletedHomeworksPages value)
        {
            return value.GetAttribute<UniqueIdentifierAttribute>().Guid;
        }
    }
}
