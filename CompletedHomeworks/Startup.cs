using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Common.Constants;
using CompletedHomeworks.Infrastructure.Extensions;
using ConfigurationProvider.Configurations.Implementations;
using ConfigurationProvider.Implementations;
using ConfigurationProvider.Providers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CompletedHomeworks
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            /*
             
              var configurationProvider = new CoreConfigurationProvider(new MicroserviceEnvironmentSettingsProvider());
            var corsSettings = configurationProvider.Provide<CorsSettings>("corsSettings", AppSettingsConstants.EnvironmentSettings);
           
           
           ...
           
           
            app.UseCors(
                options => options.WithOrigins(corsSettings.SplittedDomains).AllowAnyMethod().AllowAnyHeader()
                    .AllowCredentials());
             
             */
            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin",
                    builder => builder.WithOrigins("http://localhost:3000"));
            });
            services.RegisterConfiguration(Configuration);
            services.RegisterDependencies();
            services.RegisterMapper();
            services.AddControllersWithViews();

            /*
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
            */

            RegisterApplicationLifeTimeEvents(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var configurationProvider = new CoreConfigurationProvider(new CoreEnvironmentSettingsProvider(Configuration));
            var corsSettings = configurationProvider.Provide<CorsSettings>(AppSettingsConstants.CorsSettings);
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors(
                options => options.WithOrigins(corsSettings.SplittedDomains).AllowAnyMethod().AllowAnyHeader()
                    .AllowCredentials());
            //app.UseSpaStaticFiles();
            app.UseRouting();

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapControllerRoute(name: "default", pattern: "{controller}/{action=Index}/{id?}");
                });

            /*
            app.UseSpa(
                spa =>
                {
                    spa.Options.SourcePath = "ClientApp";

                    if (env.IsDevelopment())
                    {
                        spa.UseReactDevelopmentServer(npmScript: "run dev");
                    }
                });
                */
        }

        //TODO Move to some common api class
        private void RegisterApplicationLifeTimeEvents(IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var hostApplicationLifetime = serviceProvider.GetService<IHostApplicationLifetime>();
        }
    }
}
