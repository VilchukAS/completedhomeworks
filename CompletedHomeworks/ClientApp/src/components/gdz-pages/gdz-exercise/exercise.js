import {getExerciseInfoPage, pageName} from "../../../redux/gdz-exercise-reducer";
import {connect} from "react-redux";
import ReadOnlyExercise from "../gdz-lesson/exercise/read-only-exercise";
import {withRouter} from "react-router-dom";
import React from "react";
import Placeholder from "../../shared/gdz-preloader/preloader";
import BackButton from "../../shared/gdz-back-button/back-button";

class ExerciseContainer extends React.Component {
  componentDidMount() {
    const currentSubj = this.props.match.params.subject;
    const currentGrade = this.props.match.params.grade;
    const currentTextbook = this.props.match.params.textbook;
    const currentLesson = this.props.match.params.lesson;
    const currentExercise = this.props.match.params.exercise;
    this.props.getExerciseInfoPage(currentSubj, currentGrade, currentTextbook, currentLesson, currentExercise);
  }

  render() {
    let isFetching = this.props.isFetching;
    return (
      <>
        {
          isFetching
            ? <Placeholder/>
            :
            <>
              <ReadOnlyExercise {...this.props}/>
              <BackButton title="Назад к теме"
                          to={`/gdz/${this.props.match.params.subject}/${this.props.match.params.grade}/${this.props.match.params.textbook}/${this.props.match.params.lesson}`}/>
            </>
        }
      </>
    );
  }
}

export default connect(
  state => ({
    ...state[pageName],
    isEditable: false,
  }),
  {
    getExerciseInfoPage
  }
)(withRouter(ExerciseContainer));