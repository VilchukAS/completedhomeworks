import {gdzApi} from "../api/tutor-api";

const actionTypes = {
  TOOGLE_EXERCISE_PAGE_LOADING: 'TOOGLE_EXERCISE_PAGE_LOADING',
  SET_INFO_TO_EXERCISE_PAGE: 'SET_INFO_TO_EXERCISE_PAGE'
};

export const pageName = 'gdz-exercise';

const initValue = {
  isFetching: true
};

export const GdzExerciseReducer = (state = initValue, action) => {
    switch (action.type) {
      case actionTypes.TOOGLE_EXERCISE_PAGE_LOADING:
        return {
          ...state,
          isFetching: action.isFetching
        };
      case actionTypes.SET_INFO_TO_EXERCISE_PAGE:
        return {
          ...state,
          ...action.data
        };
      default:
        return state;
    }
  };


const isFetching = (isFetching) => {
  return {
    type: actionTypes.TOOGLE_EXERCISE_PAGE_LOADING,
    isFetching
  };
};

const setInfoToExercisePage = (data) => {
  return {
    type: actionTypes.SET_INFO_TO_EXERCISE_PAGE,
    data
  };
};

//thunk
export const getExerciseInfoPage = (subjectUrlTitle, gradeUrlTitle, textbookUrlTitle, lessonUrlTitle, exerciseUrlTitle) => {
  return (dispatch) => {
    dispatch(isFetching(true));
    gdzApi.getExerciseInfoPage(subjectUrlTitle, gradeUrlTitle, textbookUrlTitle, lessonUrlTitle, exerciseUrlTitle)
      .then(data => {
        dispatch(setInfoToExercisePage(data));
        dispatch(isFetching(false));
      })
  }
};