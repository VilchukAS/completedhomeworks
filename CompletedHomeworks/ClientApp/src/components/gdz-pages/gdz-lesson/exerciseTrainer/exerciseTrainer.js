import React from "react";
import style from "./exerciseTrainer.module.css"

const ExerciseTrainer = (props) => {
  return (
    <div className={style.exerciseTrainer}>
      {props.title}
    </div>
  )
}

export default ExerciseTrainer