import React from "react";
import {connect} from "react-redux";
import {getTextbookInfoPage, pageName} from "../../../redux/gdz-textbook-reducer";
import {lessonStatuses, routeParams} from "../../../common/constants";
import {NavLink, withRouter} from "react-router-dom";
import Utils from "../../../common/utils/utils";
import Placeholder from "../../shared/gdz-preloader/preloader";
import style from "./textbook.module.css";
import BackButton from "../../shared/gdz-back-button/back-button";

class GdzTextbookContainer extends React.Component {
  componentDidMount() {
    const currentSubj = this.props.match.params[routeParams.subject];
    const currentGrade = this.props.match.params[routeParams.grade];
    const currentTextbook = this.props.match.params[routeParams.textbook];
    this.props.getTextbookInfoPage(currentSubj, currentGrade, currentTextbook);
  }

  render() {
    const isFetching = this.props.isFetching;
    return (
      <>
        {
          isFetching
            ? <Placeholder/>
            :
            <>
              <BackButton title="Назад к классу" to={`/gdz/${this.props.match.params[routeParams.subject]}/${this.props.match.params[routeParams.grade]}`}/>
              <GdzTextbook {...this.props}/>
            </>
        }
      </>
    );
  }
}

const getCurrentLessonStyle = (status) => {
  if (status === lessonStatuses.verified) {
    return style.verifiedLesson;
  } else if (status === lessonStatuses.notVerified) {
    return style.notVerifiedLesson;
  }
  return style.emptyLesson;
};

const GdzTextbook = (props) => {
  const currentSubjectUrl = props.match.params[routeParams.subject];
  const currentGradeUrl = props.match.params[routeParams.grade];
  const currentTextbookUrl = props.match.params[routeParams.textbook];
  const lessonThemeItems = props.lessons.map(lesson => {
      let currentLessonStyle = getCurrentLessonStyle(lesson.status);

      return (
        <div key={lesson.id} className={currentLessonStyle}>
          <NavLink key={lesson.id}
                   to={Utils.combineUrl(currentSubjectUrl, currentGradeUrl, currentTextbookUrl, lesson.urlTitle)}>
            {lesson.lessonNumber}. {lesson.title}
          </NavLink>
        </div>)
    }
  );
  return (
    <div>
      <h3>Учебник: "{props.textbookTitle}", {props.author}</h3>

      {lessonThemeItems}
    </div>
  );
};

export default connect(
  state => ({
    ...state[pageName]
  }),
  {
    getTextbookInfoPage
  }
)(withRouter(GdzTextbookContainer));