﻿using AutoMapper;

using BusinesLogicLayer.Entities.Api;
using BusinesLogicLayer.Interfaces.Api;
using Common.Common.Dependency;
using ConfigurationProvider.Configurations.Implementations;

namespace BusinesLogicLayer.Implementation.Api
{
    [InterfaceDependency]
    public class ApiSettingsProvider : IApiSettingsProvider
    {
        private readonly ApiConfiguration configuration;

        private readonly IMapper mapper;

        public ApiSettingsProvider(ApiConfiguration configuration, IMapper mapper)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }

        public ApiSettings GetApiSettings()
        {
            return mapper.Map<ApiSettings>(configuration);
        }
    }
}
