﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Common.Attributes
{
    public class UniqueIdentifierAttribute : Attribute
    {
        public string Guid { get; set; }
    }
}
