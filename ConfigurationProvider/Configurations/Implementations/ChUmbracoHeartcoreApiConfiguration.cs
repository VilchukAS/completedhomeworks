﻿namespace ConfigurationProvider.Configurations.Implementations
{
    public class ChUmbracoHeartcoreApiConfiguration
    {
        public string ProjectAlias { get; set; }
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
        public string GetByIdUrl { get; set; }
    }
}