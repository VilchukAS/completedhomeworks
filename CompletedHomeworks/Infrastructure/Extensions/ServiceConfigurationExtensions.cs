﻿using AutoMapper;
using BusinesLogicLayer.Entities.AutoMapperProfiles;
using BusinesLogicLayer.Implementation;
using BusinesLogicLayer.Implementation.Api;
using BusinesLogicLayer.Interfaces;
using BusinesLogicLayer.Interfaces.Api;
using Common;
using Common.Common;
using Common.Common.Constants;
using Common.Common.Interfaces;
using ConfigurationProvider.Configurations.Implementations;
using ConfigurationProvider.Implementations;
using ConfigurationProvider.Providers;
using Core.Api.ChUmbracoHeartcore;
using Core.Api.ChUmbracoHeartcore.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CompletedHomeworks.Infrastructure.Extensions
{
    public static class ServiceConfigurationExtensions
    {
        public static void RegisterConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            var configurationProvider =
                new CoreConfigurationProvider(new CoreEnvironmentSettingsProvider(configuration));

            services.AddScoped(x =>
                configurationProvider.Provide<ChUmbracoHeartcoreApiConfiguration>(
                    AppSettingsConstants.ChUmbracoHeartcoreApiConfiguration,
                    AppSettingsConstants.ApiSettings));
        }

        public static void RegisterDependencies(this IServiceCollection services)
        {
            services.AddSingleton<IApiSettingsProvider, ApiSettingsProvider>();
            services.AddScoped<IRestClientFactory, RestClientFactory>();
            services.AddScoped<IUmbracoHeartcoreRequestManager, UmbracoHeartcoreRequestManager>();
            services.AddScoped<IHomeWorkService, HomeWorkService>();
        }

        public static void RegisterMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new ApiMapperProfile()); });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}