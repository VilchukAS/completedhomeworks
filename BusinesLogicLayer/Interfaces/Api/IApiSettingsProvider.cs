﻿using BusinesLogicLayer.Entities.Api;

namespace BusinesLogicLayer.Interfaces.Api
{
    public interface IApiSettingsProvider
    {
        ApiSettings GetApiSettings();
    }
}
