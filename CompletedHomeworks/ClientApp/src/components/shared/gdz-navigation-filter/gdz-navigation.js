import React, {useEffect, useState} from "react";
import {gdzApi} from "../../../api/tutor-api";
import {DropdownButton, Dropdown, ButtonGroup} from "react-bootstrap";
import style from "./gdz-navigation.module.css";
import Utils from "../../../common/utils/utils";
import {NavLink} from "react-router-dom";

const GdzNavigationContainer = () => {
  let [initData, setInitData] = useState({});
  let [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    gdzApi.getInitInfoForNavigation()
      .then(response => {
        setInitData(_privateFunctions.mapInitData(response));
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      {
        !isLoading &&
        <Navigation {...initData}/>
      }
    </>
  );
};

const Navigation = (props) => {
  const gradesElements = props.grades.map(grade => {
    const subjectElements = grade.subjects.map(subject => {
      return (
        <Dropdown.Item key={subject.id}>
          <NavLink to={subject.url}>{subject.title}</NavLink>
        </Dropdown.Item>
      )
    });
    return (
      <DropdownButton key={grade.id} className={style.button} as={ButtonGroup} title={grade.title} variant="secondary">
        {subjectElements}
      </DropdownButton>
    );
  });

  return (
    <div>
      <ButtonGroup className={style.buttonGroup}>
        {gradesElements}
      </ButtonGroup>
    </div>
  );
};

const _privateFunctions = {
  mapInitData: (data) => {
    const grades = data.gradesToSubjects.map(gts => {
      const grade = Utils.getById(data.grades, gts.gradeId);
      const subjects = Utils.getByIds(data.subjects, gts.subjectIds).map(subject => ({
        ...subject,
        url: Utils.combineUrl(subject.urlTitle, grade.urlTitle)
      }));
      return {
        ...grade,
        subjects
      }
    });
    return {grades};
  }
};

export default GdzNavigationContainer;