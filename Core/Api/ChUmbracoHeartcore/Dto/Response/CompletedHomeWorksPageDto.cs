﻿using Newtonsoft.Json;

namespace Core.Api.ChUmbracoHeartcore.Dto.Response
{
    public class CompletedHomeWorksPageDto : DefaultUmbracoPageDto
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("personName")]
        public string PersonName { get; set; }

        [JsonProperty("personSAge")]
        public long PersonSAge { get; set; }
    }
}