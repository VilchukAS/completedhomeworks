﻿namespace Common.Common.Constants
{
    public class CommonConstants
    {
        public const string ApiKeyHeader = "Api-Key";

        public const string UmbProjectAliasHeader = "Umb-Project-Alias";
    }
}
