import withBreadcrumbs from 'react-router-breadcrumbs-hoc';
import React from "react";
import {pageUrls} from "../../../common/constants";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {pageName as subjectPageName} from "../../../redux/gdz-subject-reducer";
import {pageName as gradePageName} from "../../../redux/gdz-grade-reducer";
import {pageName as textbookPageName} from "../../../redux/gdz-textbook-reducer";
import {pageName as lessonPageName} from "../../../redux/gdz-lesson-reducer";
import {pageName as exercisePageName} from "../../../redux/gdz-exercise-reducer";
import Utils from "../../../common/utils/utils";

const DynamicSubjectBreadcrumb = ({subjectTitle}) => <>{subjectTitle}</>;
const ConnectedToStoreDynamicSubjectBreadcrumb = connect(state => ({
  subjectTitle: Utils.isDef(state[subjectPageName].subjectTitle) ? state[subjectPageName].subjectTitle : "Учебник"
}))(DynamicSubjectBreadcrumb);

const DynamicGradeBreadcrumb = ({gradeTitle}) => <>{gradeTitle}</>;
const ConnectedToStoreDynamicGradeBreadcrumb = connect(state => ({
  gradeTitle: Utils.isDef(state[gradePageName].gradeTitle)
    ? state[gradePageName].gradeTitle
    : Utils.isDef(state[textbookPageName].gradeTitle) ? state[textbookPageName].gradeTitle : "Класс"
}))(DynamicGradeBreadcrumb); //pageName ???

const DynamicTextbookBreadcrumb = ({textbookTitle}) => <>{textbookTitle}</>;
const ConnectedToStoreDynamicTextbookBreadcrumb = connect(state => ({
  textbookTitle: Utils.isDef(state[textbookPageName].textbookTitle) ? state[textbookPageName].textbookTitle : "Учебник"
}))(DynamicTextbookBreadcrumb);

const DynamicLessonBreadcrumb = ({lessonTitle}) => <>{lessonTitle}</>;
const ConnectedToStoreDynamicLessonBreadcrumb = connect(state => ({
  lessonTitle: Utils.isDef(state[lessonPageName].lessonTitle) ? state[lessonPageName].lessonTitle : "Тема"
}))(DynamicLessonBreadcrumb);

const DynamicExerciseBreadcrumb = ({exerciseTitle}) => <>{exerciseTitle}</>;
const ConnectedToStoreDynamicExerciseBreadcrumb = connect(state => ({
  exerciseTitle: Utils.isDef(state[exercisePageName].exerciseTitle) ? state[exercisePageName].exerciseTitle : "Упражнение"
}))(DynamicExerciseBreadcrumb);

const routes = [
  {path: '/', breadcrumb: null},
  {path: pageUrls.URL_GDZ_BASE, breadcrumb: 'ГДЗ'},
  {path: pageUrls.URL_SUBJECT, breadcrumb: ConnectedToStoreDynamicSubjectBreadcrumb},
  {path: pageUrls.URL_GRADE, breadcrumb: ConnectedToStoreDynamicGradeBreadcrumb},
  {path: pageUrls.URL_TEXTBOOK, breadcrumb: ConnectedToStoreDynamicTextbookBreadcrumb},
  {path: pageUrls.URL_LESSON, breadcrumb: ConnectedToStoreDynamicLessonBreadcrumb},
  {path: pageUrls.URL_EXERCISE, breadcrumb: ConnectedToStoreDynamicExerciseBreadcrumb},
];

const Breadcrumbs = ({breadcrumbs}) => {
  const breadcrumbElements = breadcrumbs.map(({match, breadcrumb}, i) => {
    return (
      <span key={match.url}>
          <NavLink to={match.url}>{breadcrumb}</NavLink>{(i !== breadcrumbs.length - 1) && ' / '}
        </span>
    )
  });

  return (
    <div>
      {breadcrumbElements}
    </div>
  );
};

export default withBreadcrumbs(routes)(Breadcrumbs);