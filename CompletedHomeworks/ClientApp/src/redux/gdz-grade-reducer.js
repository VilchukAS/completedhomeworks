import {gdzApi} from "../api/tutor-api";

const actionTypes = {
  TOOGLE_PAGE_LOADING: "TOOGLE_GRADE_PAGE_LOADING",
  SET_INFO_TO_GRADE_PAGE: "SET_INFO_TO_GRADE_PAGE"
};

export const pageName = 'gdz-grade';

const initialValue = {
  isFetching: true
};

export const GdzGradeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case actionTypes.TOOGLE_PAGE_LOADING:
      return {
        ...state,
        isFetching: action.isFetching
      };
    case actionTypes.SET_INFO_TO_GRADE_PAGE:
      return {
        ...state,
        ...action.data
      };
    default:
      return state;
  }
};

const isFetching = (isFetching) => {
  return {
    type: actionTypes.TOOGLE_PAGE_LOADING,
    isFetching
  };
};

const setInfoToGradePage = (data) => {
  return {
    type: actionTypes.SET_INFO_TO_GRADE_PAGE,
    data
  };
};

export const getInfoForGradePage = () => {
  return (dispatch) => {
    dispatch(isFetching(true));
    gdzApi.getInfoForGradePage()
      .then(data => {
        dispatch(setInfoToGradePage(data));
        dispatch(isFetching(false));
      })
  }
};