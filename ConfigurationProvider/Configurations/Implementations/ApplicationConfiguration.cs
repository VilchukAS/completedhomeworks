﻿namespace ConfigurationProvider.Configurations.Implementations
{
    public class ApplicationConfiguration
    {
        public string Environment { get; set; }

        public string DomainUrl { get; set; }
    }
}
