import React from "react";
import style from './navbar.module.css'
import {NavLink} from "react-router-dom";
import {pageUrls} from "../../common/constants";

const Navbar = () => {
  return (
    <div className={style.navigationPanel}>
      <div className={style.item}>
        <NavLink className={style.navLinkItem} to={pageUrls.URL_GDZ_BASE}>GDZ</NavLink>
      </div>
    </div>
  );
}

export default Navbar;