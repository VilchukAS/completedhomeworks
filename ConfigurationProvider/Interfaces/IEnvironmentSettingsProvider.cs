﻿namespace ConfigurationProvider.Interfaces
{
    public interface IEnvironmentSettingsProvider
    {
        string GetEnvironment();
    }
}