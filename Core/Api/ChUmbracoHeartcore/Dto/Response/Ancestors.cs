﻿using System;
using Newtonsoft.Json;

namespace Core.Api.ChUmbracoHeartcore.Dto.Response
{
    public class Ancestors
    {
        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }
    }
}