﻿using Common.Common.Attributes;

namespace Common.Common.Enums
{
    public enum CompletedHomeworksPages
    {
        [UniqueIdentifier(Guid = "101d8cec-0317-41f0-8bab-ce5c093ea468")]
        CompleteHomework = 1,

        [UniqueIdentifier(Guid = "9ee175ba-13d5-412b-b165-ec971f26363a")]
        Lesson = 2
    }
}