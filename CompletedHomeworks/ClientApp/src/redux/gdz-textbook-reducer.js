import {lessonStatuses} from "../common/constants";
import {gdzApi} from "../api/tutor-api";

const actionTypes = {
  TOOGLE_TEXTBOOK_PAGE_LOADING: "TOOGLE_TEXTBOOK_PAGE_LOADING",
  SET_INFO_TO_TEXTBOOK_PAGE: "SET_INFO_TO_TEXTBOOK_PAGE"
};

export const pageName = 'gdz-textbook';

const initValue = {
  isFetching: true
};

export const GdzTextbookReducer = (state = initValue, action) => {
  switch (action.type) {
    case actionTypes.TOOGLE_TEXTBOOK_PAGE_LOADING:
      return {
        ...state,
        isFetching: action.isFetching
      };
    case actionTypes.SET_INFO_TO_TEXTBOOK_PAGE:
      return {
        ...state,
        ...action.data
      };
    default:
      return state;
  }
};

const isFetching = (isFetching) => {
  return {
    type: actionTypes.TOOGLE_TEXTBOOK_PAGE_LOADING,
    isFetching
  };
};

const setInfoForTextbookPage = (data) => {
  return {
    type: actionTypes.SET_INFO_TO_TEXTBOOK_PAGE,
    data
  }
};

export const getTextbookInfoPage = (subjectUrlTitle, gradeUtlTitle, textbookUrlTitle) => {
  return (dispatch) => {
    dispatch(isFetching(true));
    gdzApi.getInfoForTextbookPage(subjectUrlTitle, gradeUtlTitle, textbookUrlTitle)
      .then(data => {
        dispatch(setInfoForTextbookPage(data));
        dispatch(isFetching(false));
      });
  }
};