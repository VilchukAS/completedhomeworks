﻿namespace Common.Common.Dependency
{
    public enum DependencyScope
    {
        None = 0,

        Singleton = 1
    }
}