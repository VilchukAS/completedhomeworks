﻿using System.Net;
using Common.Common.Constants;
using Common.Common.Dependency;
using Common.Common.Enums;
using Common.Common.Extensions;
using Common.Common.Interfaces;
using ConfigurationProvider.Configurations.Implementations;
using Core.Api.ChUmbracoHeartcore.Dto.Response;
using Core.Api.ChUmbracoHeartcore.Interfaces;
using Newtonsoft.Json;
using RestSharp;

namespace Core.Api.ChUmbracoHeartcore
{
    [InterfaceDependency]
    public class UmbracoHeartcoreRequestManager : IUmbracoHeartcoreRequestManager
    {
        private readonly ChUmbracoHeartcoreApiConfiguration configuration;

        private readonly IRestClientFactory restClientFactory;

        public UmbracoHeartcoreRequestManager(ChUmbracoHeartcoreApiConfiguration configuration,
            IRestClientFactory restClientFactory)
        {
            this.configuration = configuration;
            this.restClientFactory = restClientFactory;
        }

        public DefaultUmbracoPageDto GetPageContent(CompletedHomeworksPages page)
        {
            var url = string.Format($"{configuration.BaseUrl}{configuration.GetByIdUrl}",
                page.GetCarrierUniqueIdentifier());

            var restRequest = new RestRequest(Method.GET);

            restRequest.AddHeader(CommonConstants.ApiKeyHeader, configuration.ApiKey);
            restRequest.AddHeader(CommonConstants.UmbProjectAliasHeader, configuration.ProjectAlias);

            var client = restClientFactory.Create(url);

            var restResponse = client.Execute(restRequest);

            if (restResponse.StatusCode != HttpStatusCode.OK)
            {
                //Log exeption ?
                return new DefaultUmbracoPageDto();
            }

            var response = JsonConvert.DeserializeObject<CompletedHomeWorksPageDto>(restResponse.Content);
            if (response == null)
            {
                //log ?
                return new CompletedHomeWorksPageDto();
            }

            return response;
        }
    }
}