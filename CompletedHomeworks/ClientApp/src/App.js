import React from 'react';
import style from './App.module.css';
import Navbar from "./components/navibar/navbar";
import Footer from "./components/footer/footer";
import Routes from "./routes";
import {Provider} from "react-redux";
import store from "./redux/redux-store";
import GdzNavigation from "./components/shared/gdz-navigation-filter/gdz-navigation";
import Breadcrumbs from "./components/shared/gdz-breadcrumb/gdz-breadcrumb";

const App = () => {
  return (
    <div className={style.bodyContainer}>
      <Provider store={store}>
        <div className={style.headerContainer}>
          <Navbar/>
        </div>
        <GdzNavigation/>
        <Breadcrumbs/>
        <div className={style.contentContainer}>
          <Routes/>
        </div>
        <div className={style.footerContainer}>
          <Footer/>
        </div>
      </Provider>
    </div>
  );
};

export default App;
