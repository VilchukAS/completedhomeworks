import React from "react";
import {getInfoForGradePage, pageName} from "../../../redux/gdz-grade-reducer";
import {connect} from "react-redux";
import {NavLink, withRouter} from "react-router-dom";
import {routeParams} from "../../../common/constants";
import Placeholder from "../../shared/gdz-preloader/preloader";
import style from "./gdz-grade.module.css";
import BackButton from "../../shared/gdz-back-button/back-button";

class GdzGradeContainer extends React.Component {
  componentDidMount() {
    let currentSubj = this.props.match.params[routeParams.subject];
    let currentGrade = this.props.match.params[routeParams.grade];
    this.props.getInfoForGradePage(currentSubj, currentGrade)
  }

  render() {
    let isFetching = this.props.isFetching;
    return (
      <>
        {
          isFetching
            ? <Placeholder/>
            : <>
            <BackButton title="Назад к предмету" to={`/gdz/${this.props.match.params[routeParams.subject]}`}/>
              <GdzGrade {...this.props}/>
          </>
        }
      </>
    );
  }
}

const GdzGrade = (props) => {
  const textbookItems = props.textbooks.map(textbook =>
    <div key={textbook.id} className={style.textbook}>
      <div className={`${style.textbookImg} ${style.inline}`}>
        <img src={textbook.textbookImgSrc}/>
      </div>
      <div className={`${style.inline}`}>
        <div className={style.textbookTitle}>
          <NavLink key={textbook.id}
                   to={`${props.match.url}/${textbook.urlTitle}`}>
            {textbook.title}
          </NavLink>
        </div>
        <div className={style.textbookAuthor}>{textbook.author}</div>
      </div>
    </div>
  );
  return (
    <div>
      <h3>{props.pageHeader}</h3>

      <div className={style.textbookContainer}>
        {textbookItems}
      </div>
    </div>
  );
};

export default connect(
  state => ({
    ...state[pageName]
  }),
  {
    getInfoForGradePage
  })(withRouter(GdzGradeContainer));