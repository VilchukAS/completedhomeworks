import React, {useEffect} from "react";
import {account, userRoles} from "../../../common/constants";
import {
  addEmptyExerciseToLesson,
  getLessonInfoPage,
  pageName,
  saveLesson,
  updateExerciseAdvice,
  updateExerciseCondition,
  updateExerciseSolution,
  updateExerciseTitle
} from "../../../redux/gdz-lesson-reducer";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Placeholder from "../../shared/gdz-preloader/preloader";
import {compose} from "redux";
import EditableLesson from "./editable-lesson";
import ReadOnlyLesson from "./read-only-lesson";
import style from "./lesson.module.css";
import Utils from "../../../common/utils/utils";

const LessonContainer = (props) => {
  useEffect(() => {
    props.getLessonInfoPage(props.match.params.subject, props.match.params.grade, props.match.params.textbook, props.match.params.lesson);
  }, []);

  return (
    <>
      {
        props.isFetching
          ? <Placeholder/>
          :
          <div className={style.lesson}>
            <h3>Тема урока: "{props.title}"</h3>
            <div>Предмет: {props.subjectTitle}</div>
            <div>Класс: {props.gradeTitle}</div>
            <div>Учебник: {props.textbookTitle}</div>
            {
              props.isExerciseEditable
                ? <EditableLesson onSubmit={saveLesson} {...props}/>
                : <ReadOnlyLesson {...props}/>
            }
          </div>
      }
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state[pageName],
    isExerciseEditable: account.role == userRoles.Teacher || account.role == userRoles.Methodist,
  }
};

export default compose(
  connect(mapStateToProps,
    {
      addEmptyExerciseToLesson,
      updateExerciseTitle,
      updateExerciseCondition,
      updateExerciseSolution,
      updateExerciseAdvice,
      getLessonInfoPage,
      saveLesson
    }),
  withRouter
)(LessonContainer);