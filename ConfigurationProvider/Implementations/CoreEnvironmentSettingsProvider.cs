﻿using Microsoft.Extensions.Configuration;

namespace ConfigurationProvider.Implementations
{
    public class CoreEnvironmentSettingsProvider : BaseEnvironmentSettingsProvider
    {
        private IConfiguration configuration;

        public CoreEnvironmentSettingsProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        protected override string GetConfigurationValue(string key)
        {
            return configuration[key];
        }
    }
}