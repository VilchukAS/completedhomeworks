import React from "react";
import style from "./exercise.module.css";
import {Field} from "redux-form";

const renderField = ({input, label, type, meta: {touched, error}}) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} type={type} placeholder={label}/>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
);

// };

// const InputField = (props) => {
//   delete props.input.value;
//
//   return (
//     <input
//       {...props.input}
//       {...props.file}
//       name={props.name}
//       type="file"
//       accept="image/*"
//       onChange={props.onChange}
//     />
//   )
// };

const EditableExercise = (props) => {
  return (
    <div className={style.exercise}>
      <button type="button" title="Удалить" onClick={props.removeItself}>Удалить задачу</button>
      <Field name={`${props.member}.exerciseTitle`} type="text" component={renderField} label="Название задачи"
             placeholder="Введите название задачи"/>
      <Field name={`${props.member}.condition`} type="text" component={renderField} label="Условие"
             placeholder="Введите условие задачи"/>

      <input
        name={`${props.member}.conditionPhoto`}
        type="file"
        accept="image/*"
        onChange={(event) => {
          props.setFieldValue(`${props.member}.conditionPhoto`, event.currentTarget.files[0])
        }}
        {...props.conditionPhoto}
      />

      {/*<Field name={`${props.member}.conditionPhoto`}*/}
      {/*       file={props.conditionPhoto}*/}
      {/*       accept="image/*"*/}
      {/*       onChange={(event) => {*/}
      {/*         debugger;*/}
      {/*         props.setFieldValue(`${props.member}.conditionPhoto`, event.currentTarget.files[0])*/}
      {/*       }}*/}
      {/*       component={InputField}/>*/}


      <Field name={`${props.member}.solution`} type="text" component={renderField} label="Решение"
             placeholder="Введите решение задачи"/>
      <Field name={`${props.member}.advice`} type="text" component={renderField} label="Совет"
             placeholder="Введите совет задачи"/>
    </div>
  );
};

export default EditableExercise