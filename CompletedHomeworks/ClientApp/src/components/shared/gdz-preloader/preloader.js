import React from "react";
import preloaderImage from "../../../assets/images/preloader-animation.svg"
import style from "./placeholder.module.css"

const Preloader  = () => {
  return (
    <div className={style.placeholder}>
      <img src={preloaderImage} alt="Waiting"/>
    </div>  
  );
};

export default Preloader;