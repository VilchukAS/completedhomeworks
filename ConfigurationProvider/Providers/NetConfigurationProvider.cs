﻿using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Hosting;

using ConfigurationProvider.Interfaces;

namespace ConfigurationProvider.Providers
{
    public class NetConfigurationProvider : BaseConfigurationProvider
    {
        public NetConfigurationProvider(IEnvironmentSettingsProvider environmentSettingsProvider)
            : base(environmentSettingsProvider)
        {
        }

        protected override string BasePath =>
            HostingEnvironment.IsHosted
                ? Path.Combine(HttpRuntime.AppDomainAppPath, "bin")
                : Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    }
}
