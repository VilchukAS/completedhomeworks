import style from "./lesson.module.css";
import {NavLink} from "react-router-dom";
import ReadonlyExercise from "./exercise/read-only-exercise";
import ExerciseTrainer from "./exerciseTrainer/exerciseTrainer";
import Video from "./video/video";
import ExerciseResult from "./exerciseResult/exerciseResult";
import React from "react";

const ReadOnlyLesson = (props) => {
  const exerciseElements = props.exercises.map(
    (exercise) => {
      return <div key={exercise.id} className={style.exerciseContainer}>
        <NavLink to={`${props.match.url}/${exercise.urlTitle}`}>Задача "{exercise.exerciseTitle}"</NavLink>

        <ReadonlyExercise {...exercise}/>
      </div>
    }
  );

  const exerciseTrainerElements = props.exerciseTrainers.map(
    (exerciseTrainer) => {
      return <ExerciseTrainer key={exerciseTrainer.id} {...exerciseTrainer}/>
    }
  );

  return (
    <>
      {exerciseElements}
      <div className="mt-3">Видео по этой теме:</div>
      <Video videoLink={props.videoLink} isExerciseEditable={false}/>
      {exerciseTrainerElements}
      <ExerciseResult exerciseResultInfo={props.exerciseResult}/>
    </>
  )
};

export default ReadOnlyLesson;