import React from "react";
import style from "./video.module.css"
import ReactPlayer from "react-player";

const Video = (props) => {
  return (
    <div className={style.video}>
      <ReactPlayer url={props.videoLink}/>
    </div>
  )
};

export default Video