﻿using Newtonsoft.Json;

namespace Core.Api.ChUmbracoHeartcore.Dto.Response
{
    public class Parent
    {
        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public string Href { get; set; }

        [JsonProperty("templated", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Templated { get; set; }
    }
}