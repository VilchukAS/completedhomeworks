import React from "react";
import {getInfoForSubjectPage, pageName} from "../../../redux/gdz-subject-reducer";
import {connect} from "react-redux";
import Grade from "./grade/grade"
import {routeParams} from "../../../common/constants";
import Placeholder from "../../shared/gdz-preloader/preloader";
import {withRouter} from "react-router-dom";
import GdzFilter from "../../shared/gdz-navigation-filter/gdz-navigation";
import BackButton from "../../shared/gdz-back-button/back-button";

class GdzSubjectContainer extends React.Component {
  componentDidMount() {
    let currentSubj = this.props.match.params[routeParams.subject];
    this.props.getInfoForSubjectPage(currentSubj)
  }

  render() {
    let isFetching = this.props.isFetching;
    return (
      <>
        {
          isFetching
            ? <Placeholder />
            :
            <>
              <BackButton title="Вернуться на главную" to="/gdz"/>
              <GdzSubject {...this.props}/>
            </>
        }
        </>
    );
  }
}

const GdzSubject = (props) => {
  const currentSubjectUrl = props.match.params[routeParams.subject]
  const elements = props.grades.map(grade => {
    return <Grade key={grade.id} currentSubjectUrl={currentSubjectUrl} {...grade}/>;
  });

  return (
    <div>
      <h3>Учебники по предмету "{props.subjectTitle}"</h3>

      {elements}
    </div>
  );
};

export default connect(
  state => ({
    ...state[pageName]
  }),
  {
    getInfoForSubjectPage
  })(withRouter(GdzSubjectContainer));