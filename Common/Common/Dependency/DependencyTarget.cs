﻿namespace Common.Common.Dependency
{
    public enum DependencyTarget
    {
        None = 0,

        Self = 100,

        To = 200,

        Interface = 300
    }
}