﻿using System.Configuration;

namespace ConfigurationProvider.Implementations
{
    public class EnvironmentSettingsProvider : BaseEnvironmentSettingsProvider
    {
        protected override string GetConfigurationValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}