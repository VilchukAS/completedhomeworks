﻿namespace Common.Common.Constants
{
    public class AppSettingsConstants
    {
        public const string ApplicationSettings = "applicationSettings";

        public const string ApiSettings = "apiSettings";

        public static readonly string ChUmbracoHeartcoreApiConfiguration = $"chUmbracoHeartcoreApiConfiguration"; 
        
        public const string CorsSettings = "corsSettings";
    }
}