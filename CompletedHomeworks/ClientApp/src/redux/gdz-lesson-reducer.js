import {gdzApi} from "../api/tutor-api";

const actionTypes = {
  ADD_EMPTY_EXERCISE_TO_LESSON: 'ADD_EMPTY_EXERCISE_TO_LESSON',
  CHANGE_EXERCISE_CONDITION: 'CHANGE_EXERCISE_CONDITION',
  CHANGE_EXERCISE_TITLE: 'CHANGE_EXERCISE_TITLE',
  CHANGE_EXERCISE_SOLUTION: 'CHANGE_EXERCISE_SOLUTION',
  CHANGE_EXERCISE_ADVISE: 'CHANGE_EXERCISE_ADVISE',
  TOOGLE_LESSON_PAGE_LOADING: 'TOOGLE_LESSON_PAGE_LOADING',
  SET_INFO_TO_LESSON_PAGE: 'SET_INFO_TO_LESSON_PAGE',
  SET_LESSON_DATA_AFTER_SAVING: 'SET_LESSON_DATA_AFTER_SAVING'
};

export const pageName = 'gdz-lesson';

const initValue = {
  isFetching: true
};

export const GdzLessonReducer = (state = initValue, action) => {
  switch (action.type) {
    case actionTypes.TOOGLE_LESSON_PAGE_LOADING:
      return {
        ...state,
        isFetching: action.isFetching
      };
    case actionTypes.SET_INFO_TO_LESSON_PAGE:
      return {
        ...state,
        ...action.data
      };
    case actionTypes.SET_LESSON_DATA_AFTER_SAVING:
      return {
        ...state,
        ...action.formData
      };
    default:
      return state;
  }
};

export const addEmptyExerciseToLesson = () => {
  return {
    type: actionTypes.ADD_EMPTY_EXERCISE_TO_LESSON
  };
};

export const updateExerciseTitle = (newValue, exerciseId) => {
  return {
    type: actionTypes.CHANGE_EXERCISE_TITLE,
    newValue: newValue,
    exerciseId: exerciseId
  };
};

export const updateExerciseCondition = (newValue, exerciseId) => {
  return {
    type: actionTypes.CHANGE_EXERCISE_CONDITION,
    newValue: newValue,
    exerciseId: exerciseId
  };
};

export const updateExerciseSolution = (newValue, exerciseId) => {
  return {
    type: actionTypes.CHANGE_EXERCISE_SOLUTION,
    newValue: newValue,
    exerciseId: exerciseId
  };
};

export const updateExerciseAdvice = (newValue, exerciseId) => {
  return {
    type: actionTypes.CHANGE_EXERCISE_ADVISE,
    newValue: newValue,
    exerciseId: exerciseId
  };
};

const isFetching = (isFetching) => {
  return {
    type: actionTypes.TOOGLE_LESSON_PAGE_LOADING,
    isFetching
  };
};

const setInfoToLessonPage = (data) => {
  return {
    type: actionTypes.SET_INFO_TO_LESSON_PAGE,
    data
  };
};

const setLessonDataAfterSaving = (formData) => {
  return {
    type: actionTypes.SET_LESSON_DATA_AFTER_SAVING,
    formData
  };
};

//thunk
export const getLessonInfoPage = (subjectUrlTitle, gradeUrlTitle, textbookUrlTitle, lessonUrlTitle) => {
  return (dispatch) => {
    dispatch(isFetching(true));
    gdzApi.getLessonInfoPage(subjectUrlTitle, gradeUrlTitle, textbookUrlTitle, lessonUrlTitle)
      .then(data => {
        dispatch(setInfoToLessonPage(data));
        dispatch(isFetching(false));
      })
  }
};

export const saveLesson = (formData) => {
  debugger;
  return (dispatch) => {
    dispatch(isFetching(true));
    gdzApi.saveLesson(formData)
      .then(data => {
        if (data.resultCode == 200) {
          dispatch(setLessonDataAfterSaving(formData));
        }
        dispatch(isFetching(false));
      })
  }
};