﻿using System;
using System.Collections.Generic;
using System.Text;
using BusinesLogicLayer.ModelsDto.Gdz;

namespace BusinesLogicLayer.Interfaces
{
    public interface IHomeWorkService
    {
        bool IsLessonDefined(int lessonId);

        GdzTableInfoDto GetGdzTableInfo();
    }
}
