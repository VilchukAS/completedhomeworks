﻿namespace BusinesLogicLayer.ModelsDto.Gdz
{
    public class GradeDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string UrlTitle { get; set; }
        public bool IsActive { get; set; }
    }
}