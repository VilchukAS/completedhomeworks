﻿using System;
using Newtonsoft.Json;

namespace Core.Api.ChUmbracoHeartcore.Dto.Response
{
    public class DefaultUmbracoPageDto
    {
        [JsonProperty("_creatorName", NullValueHandling = NullValueHandling.Ignore)]
        public string CreatorName { get; set; }

        [JsonProperty("_url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonProperty("_writerName", NullValueHandling = NullValueHandling.Ignore)]
        public string WriterName { get; set; }

        [JsonProperty("_hasChildren", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasChildren { get; set; }

        [JsonProperty("_level", NullValueHandling = NullValueHandling.Ignore)]
        public long? Level { get; set; }

        [JsonProperty("_createDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreateDate { get; set; }

        [JsonProperty("_id", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? Id { get; set; }

        [JsonProperty("_updateDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? UpdateDate { get; set; }

        [JsonProperty("_links", NullValueHandling = NullValueHandling.Ignore)]
        public Links Links { get; set; }

        [JsonProperty("contentTypeAlias")]
        public string ContentTypeAlias { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sortOrder")]
        public long SortOrder { get; set; }
    }
}