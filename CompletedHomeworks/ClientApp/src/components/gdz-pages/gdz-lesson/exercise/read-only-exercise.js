import style from "./exercise.module.css";
import React from "react";

const ReadOnlyExercise = (props) => {
  return (
    <div className={style.exercise}>
      <h5 className={style.title}>{props.exerciseTitle}</h5>

      <h5 className="mt-2 mb-0">Условие</h5>
      <div>{props.condition}</div>

      <h5 className="mt-2 mb-0">Решение</h5>
      <div>{props.solution}</div>

      <h5 className="mt-2 mb-0">Совет</h5>
      <div>{props.advice}</div>
    </div>
  );
};

export default ReadOnlyExercise;