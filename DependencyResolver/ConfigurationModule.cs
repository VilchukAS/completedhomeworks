﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Common.Constants;
using ConfigurationProvider.Configurations.Implementations;
using ConfigurationProvider.Interfaces;
using ConfigurationProvider.Providers;
using Ninject;
using Ninject.Modules;

namespace DependencyResolver
{
    public class ConfigurationModule : NinjectModule
    {
        private NetConfigurationProvider configurationProvider;

        public override void Load()
        {
            configurationProvider = new NetConfigurationProvider(Kernel.Get<IEnvironmentSettingsProvider>());

            BindFromProvider<ApplicationConfiguration>("applicationSettings", "");
            BindFromProvider<ChUmbracoHeartcoreApiConfiguration>("chUmbracoHeartcoreApiConfiguration", AppSettingsConstants.ApiSettings);

        }

        private T GetSectionFromProvider<T>(string sectionName, string path) where T : new()
        {
            return configurationProvider.Provide<T>(sectionName, path);
        }

        private void BindFromProvider<TService, TImplementation>(string sectionName, string path) where TImplementation : TService, new()
        {
            Bind<TService>().ToMethod(x => GetSectionFromProvider<TImplementation>(sectionName, path)).InSingletonScope();
        }

        private void BindFromProvider<TConfig>(string sectionName, string path) where TConfig : new()
        {
            Bind<TConfig>().ToMethod(x => GetSectionFromProvider<TConfig>(sectionName, path)).InSingletonScope();
        }

        private void BindFromProvider<TConfig>(string sectionName, string path, string name) where TConfig : new()
        {
            Bind<TConfig>().ToMethod(x => GetSectionFromProvider<TConfig>(sectionName, path)).InSingletonScope().Named(name);
        }

        private void BindFromProvider<TService, TImplementation>(string sectionName, string path, string name) where TImplementation : TService, new()
        {
            Bind<TService>().ToMethod(x => GetSectionFromProvider<TImplementation>(sectionName, path)).InSingletonScope().Named(name);
        }
    }
}
