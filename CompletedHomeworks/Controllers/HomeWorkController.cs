﻿using System;
using BusinesLogicLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompletedHomeworks.Controllers
{
    [ApiController]
    [Route("api/v1/HomeWork")]
    public class HomeWorkController : Controller
    {
        private readonly IHomeWorkService _homeWorkService;

        public HomeWorkController(IHomeWorkService homeWorkService)
        {
            _homeWorkService = homeWorkService;
        }

        [HttpGet("filter-table")]
        public ActionResult Get()
        {
            var info = _homeWorkService.GetGdzTableInfo();
            return this.Json(info);
        }

        // [HttpGet("filtres")]
        // public ActionResult Filtres([FromQuery] int? selectedSubject, [FromQuery] int? selectedGrade, [FromQuery] int? selectedTextbook,)
        // {
        //     if (!_homeWorkService.IsLessonDefined(lessonId))
        //         return NotFound();
        //
        //     var pageContent = "pageContent";
        //     return this.Json(pageContent);
        // }

        [HttpGet("lesson/{lessonId}")]
        public ActionResult Get(int lessonId)
        {
            if (!_homeWorkService.IsLessonDefined(lessonId))
                return NotFound();

            var pageContent = "pageContent";
            return this.Json(pageContent);
        }
    }
}