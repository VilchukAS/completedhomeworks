﻿using ConfigurationProvider.Constants;

namespace ConfigurationProvider.Configurations.Implementations
{
    public class CorsSettings
    {
        public string AllowedDomains { get; set; }

        public string[] SplittedDomains => AllowedDomains.Split(ConfigurationConstants.Separator);
    }
}