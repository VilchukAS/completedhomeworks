﻿using ConfigurationProvider.Interfaces;

using Microsoft.Extensions.Configuration;

namespace ConfigurationProvider.Providers
{
    public abstract class BaseConfigurationProvider
    {
        private readonly IConfigurationRoot configuration;

        protected abstract string BasePath { get; }

        protected IConfigurationBuilder ConfigurationBuilder { get; set; }

        public BaseConfigurationProvider(IEnvironmentSettingsProvider environmentSettingsProvider)
        {
            var environment = environmentSettingsProvider.GetEnvironment();

            ConfigurationBuilder = new ConfigurationBuilder()
                .SetBasePath(BasePath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true);

            configuration = ConfigurationBuilder.Build();
        }

        public TResult Provide<TResult>(string sectionName, string path) where TResult : new()
        {
            var result = new TResult();
            var pathSection = configuration.GetSection(path);
            pathSection.Bind(sectionName, result);
            return result;
        }
        
        public TResult Provide<TResult>(string sectionName) where TResult : new()
        {
            var result = new TResult();
            configuration.Bind(sectionName, result);
            return result;
        }
    }
}